const express = require('express');

const app = express();

const ports = process.env.PORT || 3000;

const loginRoutes = require('./src/app/routes/loginRoute');

const postRoutes = require(`./src/app/routes/postRoute`);

const errorController = require('./src/app/controllers/error');

//https://stackoverflow.com/questions/47232187/express-json-vs-bodyparser-json

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use((req, res, next) => {
    res.setHeader(`Access-Control-Allow-Origin`, `*`);
    res.setHeader(`Access-Control-Allow-Methods`,
        `GET, POST, PUT, DELETE, *`);
    res.setHeader(
        `Access-Control-Allow-Headers`,
        `Content-Type, Authorization, Accept, X-Custom-Header`
    )
    next();
});

app.use('/auth', loginRoutes);
app.use('/post', postRoutes);

app.use(errorController.get404);

app.use(errorController.get500);

//listen for the port number
app.listen(ports, () => console.log(`Listening on port ${ports}`));