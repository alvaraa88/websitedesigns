const jwt = require(`jsonwebtoken`);

//https://www.syncfusion.com/blogs/post/best-practices-for-jwt-authentication-in-angular-apps.aspx

module.exports = (req, res, next) => {
    const authHeader = req.get(`Authorization`);

    if(!authHeader) {
        const error = new Error(`not authorized!`);
        error.statusCode = 401;
        throw error;
    }
    //why split? because by convention its Bearer sfssk999sf...
    const token = authHeader.split(` `)[1];

    let decodedToken;
    try{
        decodedToken = jwt.decode(token,{complete: true});
    } catch (err) {
        console.log("something is wrong with decode.")
        err.statusCode = 500;
        throw err;
    }
    if(!decodedToken) {
        const error = new Error(`Not authorized`);
        error.statusCode = 401;
        throw error;
    }

    req.isLoggedIn = true;
    req.userId = decodedToken.userId;
    req.email = decodedToken.email;
    next();
}