const express = require('express');

const { body } = require('express-validator');

const router = express.Router();

const postController = require(`../controllers/postController`);

const authMiddleware = require(`../middleware/authMiddleware`);

router.get(`/fetchMessage`, authMiddleware, postController.fetchAll);

// intercepts the body of the typed input from the signup page
// and adding it to the database via post
router.post(
    `/postMessage`,
    [
        authMiddleware,
        body(`title`).trim().isLength({min: 5}).not().isEmpty(),
        body(`body`).trim().isLength({min: 10}).not().isEmpty(),
        body(`user`).trim().not().isEmpty(),
    ],
    postController.postMessage,
);

router.delete(`/:id`, authMiddleware, postController.deletePost);

module.exports = router;