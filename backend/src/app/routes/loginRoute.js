const express = require('express');

const { body } = require('express-validator');

const router = express.Router();

const User = require('../models/user');

const loginController = require('../controllers/loginController');

router.get(`/getUsers`, loginController.getAllUsers);

// intercepts the body of the typed input from the signup page
// and adding it to the database via post
router.post(
    `/signup`,
    [
        body(`name`).trim().not().isEmpty(),
        body(`email`).isEmail()
            .withMessage(`Please enter a valid email.`)
            .custom(async (email) => {

                const user = await User.find_Email(email);
                if(user[0].length > 0){
                    return Promise.reject(`Email address already exists`);
                }
            })
            .normalizeEmail(),

        body('password').trim().isLength({min: 4})
        ],
    loginController.signup,
);

router.post('/login', loginController.login);

router.delete(`/:id`, loginController.deleteUser);

module.exports = router;