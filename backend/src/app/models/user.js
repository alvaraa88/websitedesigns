const db = require('../util/database');

module.exports = class User {

    constructor(name, email, password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    static fetchAllUsers(){
        return db.execute(`SELECT * FROM users`)
    }

    static find_Email(email) {
        return db.execute(
            `SELECT * FROM users WHERE email = ?`, [email]
        );
    }

    static user_signup(user){
        return db.execute(
            `INSERT INTO users (name, email, password) VALUES(?, ?, ?)`,
            [
                user.name,
                user.email,
                user.password
            ]
        );
    }

    static update_user(){

    }

    static deleteUser(id){
        return db.execute(`DELETE FROM users WHERE id = ?`,[id]);
    }
};