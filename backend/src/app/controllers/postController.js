const { validationResult } = require('express-validator');

const postModel = require('../models/post');

exports.postMessage = async (req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) return;

    console.log("reaching postMessage");
    const title = req.body.title;
    const body = req.body.body;
    const user = req.body.user;

    try{

        const post = {
            title: title,
            body: body,
            user: user,
        };

        const result = await postModel.save(post);
        res.status(201).json({ message: `Posted.`});
    } catch (err) {
        console.log("somethings wrong in postController");
        if(!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.fetchAll = async (req, res, next) => {
    try{
        const [allPosts] = await postModel.fetchAll();
        res.status(200).json(allPosts);
    } catch (err){
        if(!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.deletePost = async (req, res, next) => {
    try{
        const deleteResponse = await postModel.delete(req.params.id);
        res.status(200).json(deleteResponse);
    } catch (err){
        if(!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};
