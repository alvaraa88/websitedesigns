const { validationResult } = require('express-validator');

const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');

const User = require('../models/user');


//check "jwt.io" to confirm encoded key
// https://github.com/auth0/node-jsonwebtoken#tokenexpirederror


exports.getAllUsers = async (req, res, next) => {
    try{
        const [allUsers] = await User.fetchAllUsers();
        res.status(200).json(allUsers);
    } catch(err) {
        console.log("Error in GET_allusers")
        if(!err.statusCode) {
            err.statusCode = 500
        }
        next(err);
    }
};

exports.signup = async (req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) return;

    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;

    try{
        const hashedPassword = await bcrypt.hash(password, 12);

        const userDetails = {
            name: name,
            email: email,
            password: hashedPassword,
        };

        const updateResponse = await User.user_signup(userDetails);
        res.status(201).json(updateResponse);
    } catch (err) {
        console.log("somethings wrong in controller .signup");
        if(!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

//check "jwt.io" to confirm encoded key
// https://github.com/auth0/node-jsonwebtoken#tokenexpirederror
exports.login = async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    try{
        //storing user
        const user = await User.find_Email(email);
        if(user[0].length !== 1) {
            const error = new Error('An email does not exist!');
            error.statusCode = 401;
            throw error;
        }

        const storedUser = user[0][0];

        //storedUser.password is from the db
        const isEqual = await bcrypt.compare(password, storedUser.password);

        if(!isEqual){
            const error = new Error('Wrong password');
            error.statusCode = 401;
            throw error;
        }

        /**
         * takes cookie through associated storage with payload
         * takes the data, makes a secret and authenticates
         * @type {*}
         */
        const secret = 'secret';
        const token = jwt.sign({
                email: storedUser.email,
                userId: storedUser.id,
            },
            //administered by the server itself.
            secret,{ expiresIn: '7d' },
            function(err, token) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(token);
                }
            });

        res.status(200).json({ token: token, userId: storedUser.id });
    } catch (err) {
        if (!err.statusCode) {
            console.log("somethings wrong in Controller.login")
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.deleteUser = async (req, res, next) => {
    try{
        const deleteResponse = await User.deleteUser(req.params.id);
        res.status(200).json(deleteResponse);
    } catch(err) {
        if(!err.statusCode) {
            err.statusCode = 500
        }
        next(err);
    }
};