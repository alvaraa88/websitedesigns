import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
})
export class EventComponent implements OnInit {
  type = '';
  id = '';
  url = '';
  events: any;
  event: any;

  constructor(private route: ActivatedRoute, private http: HttpClient) {}

  ngOnInit(): void {
    this.type = this.route.snapshot.params['type'];
    this.id = this.route.snapshot.params['id'];
    if (this.type === 'trending') {
      this.url = 'http://localhost:4200/assets/data/trending-events.json';
    }
    if (this.type === 'live') {
      this.url = 'http://localhost:4200/assets/data/live-events.json';
    }
    if (this.type === 'popular') {
      this.url = 'http://localhost:4200/assets/data/popular-events.json';
    }
    this.getEvent();
  }

  getEvent() {
    this.http
      .get(this.url)
      .subscribe((events) => {
      this.events = events;
      let index = this.events
        .findIndex(
        (event: { id: string }) => event.id == this.id);
      if (index > -1) {
        this.event = this.events[index];
      }
    });
  }
}
