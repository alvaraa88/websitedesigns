export interface User_model {
  id: number;
  name: string;
  email: string;
  password: string;
}
