import { Component, OnInit } from '@angular/core';
import { WeatherService } from "../services/weather.service";
import { WeatherData } from "../models/Weather_model";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  cityName: string = `Albuquerque`;
  weatherData!: WeatherData;
  lat!: number;
  lon!: number;

  constructor(private weatherService: WeatherService) {}

  ngOnInit(): void {
    this.getLocation();
    this.getCity(this.cityName);
    this.cityName =``;
  }

  onSubmitCity(){
    this.getLocation();
    this.getCity(this.cityName);
    this.cityName =``;
  }

  getLocation(){
    if("geolocation" in navigator){
      navigator.geolocation.watchPosition((success) => {
        this.lat = success.coords.latitude;
        this.lon = success.coords.longitude;

        this.weatherService.getWeatherDataByCoords(this.lat, this.lon)
          .subscribe(data => {
          this.weatherData = data;
        });
      });
    }
  }

  private getCity(cityName: string){
    this.weatherService.getWeatherDataByCityName(cityName)
      .subscribe(data => {
          this.weatherData = data;
          console.log(data);
        });
  }

  public getTempInFahrenheit(kelvin: number){
    return ( (kelvin - 273.15) * 9/5) + 32;
  }

  public getTempInMetric(kelvin: number){
    return (kelvin - 273.15);
  }
}
