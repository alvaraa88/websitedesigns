import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { EventComponent } from "./event/event.component";
import { SignupComponent } from "./signup/signup.component";
import { WeatherComponent } from "./weather/weather.component";

const routes: Routes = [
  {path: ``, redirectTo: `login_page`, pathMatch: `full`},
  {path: `login_page`, component: LoginComponent},
  {path: `signup_page`, component: SignupComponent},
  {path: `home_page`, component: HomeComponent},
  {path: `event_page/:type/:id`, component: EventComponent},
  {path: `weather_page`, component: WeatherComponent},
  {path: `**`, component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
