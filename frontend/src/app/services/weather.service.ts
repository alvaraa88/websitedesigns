import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { WeatherData } from "../models/Weather_model";
import { Observable } from "rxjs";

// https://betterprogramming.pub/how-to-use-the-openweathermap-api-to-make-a-weather-app-f8d67b22c3ca
//https://openweathermap.org

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeatherDataByCoords(lat: number, lon: number){
    return this.http
      .get<WeatherData>(`${environment.weatherApiBaseUrl}/weather`,{
        params: new HttpParams()
          .set(`lat`, lat)
          .set(`lon`, lon)
          .set(`unit`, `imperial`)
          .set(`appid`, environment.apiKeyHeaderValue)
      });
  }

  /**
   * for reference URL looks like this:
   * https://api.openweathermap.org/data/2.5/weather?id={city id}&appid={API key}
   * @param cityName
   */
  getWeatherDataByCityName(cityName: string): Observable<WeatherData> {
    return this.http
      .get<WeatherData>(`${environment.weatherApiBaseUrl}/weather`,{
        params: new HttpParams()
          .set(`q`, cityName)
          .set(`appid`, environment.apiKeyHeaderValue)
          .set(`unit`, `imperial`)

      });
  }

}
