import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User_model } from "../models/User_model";
import { BehaviorSubject, Observable } from "rxjs";
import { first, catchError, tap } from "rxjs/operators";
import { ErrorHandlerService } from "./error-handler.service";

@Injectable({
  providedIn: `root`,
})
export class AuthService {
  private api_route = `http://localhost:3000/auth`;

  httpOptions: { headers: HttpHeaders } = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
  };

  userId!: Pick<User_model,`id`>;
  isUserLoggedIn$ = new BehaviorSubject<boolean>(false);

  constructor(
    private router: Router,
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService) {
  }

  login(email: Pick<User_model, "email">, password: Pick<User_model, "password"> ): Observable<{
    token: string; userId: Pick<User_model, "id"> }>
  {
    return this.http
      .post<User_model>(`${this.api_route}/login`, {email, password}, this.httpOptions)
      .pipe(
        first(),
        tap((tokenObject: any) => {
          this.userId = tokenObject.userId;
          localStorage.setItem(`token`, tokenObject.token);
          this.isUserLoggedIn$.next(true);
          this.router.navigate([`home_page`]);
        }),
        catchError(this.errorHandlerService.handleError<{
          token: string;
          userId: Pick<User_model, "id">;
        }>(`login`)
        )
      );
  }

  logout(){
    this.router
      .navigate(['login_page'])
      .then(r => console.log(r));
  }

  signup(user: Omit<User_model, `id`>): Observable<User_model>{
    return this.http
      .post<User_model>(`${this.api_route}/signup`, user, this.httpOptions)
      .pipe(
        first(),
        catchError(this.errorHandlerService.handleError<User_model>(`signup`))
      )
  };
}
