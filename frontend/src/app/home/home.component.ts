import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  trendingEvents: any;
  popularEvents: any;
  liveEvents: any;

  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit(): void {
    this.getTrendingEvents();
    this.getPopularEvents();
    this.getLiveEvents();
  }

  getTrendingEvents() {
    this.http
      .get('http://localhost:4200/assets/data/trending-events.json')
      .subscribe((events) => {
        this.trendingEvents = events;
        console.log(this.trendingEvents)
      });
  }

  getPopularEvents() {
    this.http
      .get('http://localhost:4200/assets/data/popular-events.json')
      .subscribe((events) => {
        this.popularEvents = events;
        console.log(this.popularEvents)
      });
  }

  getLiveEvents() {
    this.http
      .get('http://localhost:4200/assets/data/live-events.json')
      .subscribe((events) => {
        this.liveEvents = events;
        console.log(this.liveEvents)
      });
  }

  goToEvent(type: string, id: string){
    this.router.navigate(['event_page', type, id]);
  }

}
