import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string = "";
  password: string = "";
  errorMsg: string = "";
  loginForm!: FormGroup;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(4)])
    })}

  login(): void {
    this.auth.login(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe();
  }

  goto_Home(): void {
    this.router.navigate([`home_page`]);
  }

  goto_Signup(): void {
    this.router.navigate([`signup_page`]);
  }


}
